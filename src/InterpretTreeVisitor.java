/**
 * Acelasi lucru cu TreeVisitor simplu doar ca acesta nu va vizita
 * absolut toate nodurile (va exclude frunzele)
 * @author arbiter
 *
 */
public class InterpretTreeVisitor implements Visitor {

	private Visitor v;
	
	InterpretTreeVisitor (Visitor v){
		this.v = v;
	}
	
	/**
	 * Vizitam nodul si lista sa de noduri
	 * @param node
	 */
	private void visitNode(Node n){
		n.accept(v);
		for (Node node : n.getNodes()){
			node.accept(this);
		}
	}
	
	@Override
	public void visit(MainNode main) {
		if (!main.isVisitable())
			return;
		visitNode(main);
	}

	@Override
	public void visit(DeclareNode declare) {
		visitNode(declare);
	}

	@Override
	public void visit(LvalNode left) {
		return;
	}

	@Override
	public void visit(ConstantNode constant) {
		return;
	}

	@Override
	public void visit(AssignmentNode assignment) {
		visitNode(assignment);
	}

	@Override
	public void visit(PrintNode print) {
		visitNode(print);
	}

	@Override
	public void visit(StringNode string) {
		return;
	}

	@Override
	public void visit(VariableNode var) {
		return;
	}

	@Override
	public void visit(RvalNode right) {
		return;
	}

	@Override
	public void visit(IfNode ifNode) {
		visitNode(ifNode);
	}

	@Override
	public void visit(ConditionNode condition) {
		return;
	}

	@Override
	public void visit(IfBodyNode ifBody) {
		visitNode(ifBody);
	}

	@Override
	public void visit(ElseBodyNode elseBody) {
		visitNode(elseBody);
	}

	@Override
	public void visit(OrNode or) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(SumNode sum) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(MultiplicationNode mult) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(WhileNode whileNode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(GreaterThanNode greater) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(BodyNode body) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(EqualToNode equal) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(ModuloNode modulo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(AndNode and) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(DivisionNode div) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visit(DifferenceNode diff) {
		// TODO Auto-generated method stub

	}

}
