
public class PrintNode extends Node {
	
	PrintNode(String text){
		super("PrintNode");
		Node node;
		if (text.contains("\"")){
			text = text.replace("\"", "");
			node = new StringNode(text);
		} else{
			node = new VariableNode(text);
		}
		node.setFather(this);
		this.addNode(node);
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
