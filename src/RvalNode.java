
public class RvalNode extends Node {
	
	String varName;
	
	RvalNode(String name){
		super("RvalNode");
		this.varName = name;
	}
	
	public String getVarName (){
		return varName;
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
