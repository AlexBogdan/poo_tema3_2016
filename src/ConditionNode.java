
public class ConditionNode extends Node {
	
	String varName;
	
	ConditionNode(String varName){
		super("ConditionNode");
		this.varName = varName;
	}
	
	public String getVarName(){
		return varName;
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
