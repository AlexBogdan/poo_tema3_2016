
public class ConstantNode extends Node {

	private int varValue;
	
	ConstantNode(int value) {
		super("ConstantNode");
		varValue = value;
	}
	
	public int getVarValue(){
		return varValue;
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
