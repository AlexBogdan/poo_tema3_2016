
public class WhileNode extends Node {

	private boolean repeat = true;
	
	WhileNode(String varName){
		super("WhileNode");
		ConditionNode cond = new ConditionNode(varName);
		cond.setFather(this);
		this.addNode(cond);
		BodyNode body = new BodyNode();
		body.setFather(this);
		this.addNode(body);
	}
	
	public boolean getRepeat(){
		return repeat;
	}
	
	public void setRepeat(boolean value){
		repeat = value;
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
