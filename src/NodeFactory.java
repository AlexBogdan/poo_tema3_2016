
public class NodeFactory {
	
	private static boolean isNumeric(String str)  {  
		try {  
		  @SuppressWarnings("unused")
		  double d = Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe) {  
		  return false;  
		}  
		
		return true;  
	}
	
	private static int checkMacro(String string){
		if (string.equals("@I LIED")){
			return 0;
		}
		else if (string.equals("@NO PROBLEMO")){
			return 1;
		}
		return -1;
	}
	
	private static Node getRight(String varName){
		Node right;
		if (checkMacro(varName) >= 0){
			right = new ConstantNode(checkMacro(varName));
		}
		else if (isNumeric(varName)){
			right = new ConstantNode(Integer.parseInt(varName));
		}
		else{
			right = new RvalNode(varName);
		}
		return  right;
	}
	
	public static Node generateNode(String text){
		
		text = text.trim();
		text = text.replaceAll(" +", " ");
		
		if (text.contains("IT'S SHOWTIME")){
			ArnoldSchwarzenegger.setFlag(10);
			return new MainNode();
		}
		
		if (text.contains("YOU HAVE BEEN TERMINATED")){
			ArnoldSchwarzenegger.setFlag(-10);
			return null;
		}
		
		if (text.contains("HEY CHRISTMAS TREE")){
			ArnoldSchwarzenegger.setFlag(1);
			String varName = text.replaceFirst("HEY CHRISTMAS TREE ", "");
			return new DeclareNode(varName);
		}
		
		if (text.contains("YOU SET US UP")){
			ArnoldSchwarzenegger.setFlag(-1);
			String varValue = text.replaceFirst("YOU SET US UP ", "");
			if (checkMacro(varValue) >= 0){
				return new ConstantNode(checkMacro(varValue));
			}
			return new ConstantNode(Integer.parseInt(varValue));
		}
		
		if (text.contains("GET TO THE CHOPPER")){
			ArnoldSchwarzenegger.setFlag(1);
			String varName = text.replaceFirst("GET TO THE CHOPPER ", "");
			return new AssignmentNode(varName);
		}
		
		if (text.contains("HERE IS MY INVITATION")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("HERE IS MY INVITATION ", "");
			
			if (checkMacro(varName) >= 0){
				return new ConstantNode(checkMacro(varName));
			}
			else if (isNumeric(varName)){
				return new ConstantNode(Integer.parseInt(varName));
			}
			else{
				return new RvalNode(varName);
			}
		}
		
		if (text.contains("CONSIDER THAT A DIVORCE")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("CONSIDER THAT A DIVORCE ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			OrNode or = new OrNode();
			node.setFather(or);
			or.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(or);
			or.addNode(right);
			
			return or;
		}
		
		if (text.contains("KNOCK KNOCK")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("KNOCK KNOCK ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			AndNode and = new AndNode();
			node.setFather(and);
			and.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(and);
			and.addNode(right);
			
			return and;
		}
		
		if (text.contains("ENOUGH TALK")){
			ArnoldSchwarzenegger.setFlag(-1);
			return null;
		}
		
		if (text.contains("TALK TO THE HAND ")){
			ArnoldSchwarzenegger.setFlag(0);
			String string = text.replaceFirst("TALK TO THE HAND ", "");
			return new PrintNode(string);
		}
		
		if (text.contains("BECAUSE I'M GOING TO SAY PLEASE")){
			ArnoldSchwarzenegger.setFlag(2);
			String string = text.replaceFirst("BECAUSE I'M GOING TO SAY PLEASE ", "");
			return new IfNode(string);
		}
		
		if (text.contains("BULLSHIT")){
			ArnoldSchwarzenegger.previous();
			ArnoldSchwarzenegger.setFlag(1);
			return new ElseBodyNode();
		}
		
		if (text.contains("YOU HAVE NO RESPECT FOR LOGIC")){
			ArnoldSchwarzenegger.setFlag(-2);
			return null;
		}
		
		if (text.contains("GET UP")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("GET UP ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			SumNode sum = new SumNode();
			node.setFather(sum);
			sum.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(sum);
			sum.addNode(right);
			
			return sum;
		}
		
		if (text.contains("GET DOWN")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("GET DOWN ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			DifferenceNode diff= new DifferenceNode();
			node.setFather(diff);
			diff.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(diff);
			diff.addNode(right);
			
			return diff;
		}
		
		if (text.contains("I LET HIM GO")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("I LET HIM GO ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			ModuloNode modulo = new ModuloNode();
			node.setFather(modulo);
			modulo.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(modulo);
			modulo.addNode(right);
			
			return modulo;
		}
		
		if (text.contains("YOU'RE FIRED")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("YOU'RE FIRED ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			MultiplicationNode mult = new MultiplicationNode();
			node.setFather(mult);
			mult.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(mult);
			mult.addNode(right);
			
			return mult;
		}
		
		if (text.contains("HE HAD TO SPLIT")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("HE HAD TO SPLIT ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			DivisionNode div = new DivisionNode();
			node.setFather(div);
			div.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(div);
			div.addNode(right);
			
			return div;
		}
		
		if (text.contains("STICK AROUND")){
			ArnoldSchwarzenegger.setFlag(2);
			String varName = text.replaceFirst("STICK AROUND ", "");
			return new WhileNode(varName);
		}
		
		if (text.contains("LET OFF SOME STEAM BENNET")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("LET OFF SOME STEAM BENNET ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			GreaterThanNode greater = new GreaterThanNode();
			node.setFather(greater);
			greater.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(greater);
			greater.addNode(right);
			
			return greater;
		}
		
		if (text.contains("YOU ARE NOT YOU YOU ARE ME")){
			ArnoldSchwarzenegger.setFlag(0);
			String varName = text.replaceFirst("YOU ARE NOT YOU YOU ARE ME ", "");
			
			Node node = ArnoldSchwarzenegger.getCurrentNode().getNodes().removeLast();
			EqualToNode equal = new EqualToNode();
			node.setFather(equal);
			equal.addNode(node);
			
			Node right = getRight(varName);
			right.setFather(equal);
			equal.addNode(right);
			
			return equal;
		}
		
		if (text.contains("CHILL")){
			ArnoldSchwarzenegger.setFlag(-2);
			return null;
		}
		
		if (text.equals(""))
			return null;
		
		//System.out.println("[S-a dat dracu] Nu s-a gasit niciun nodeType!'" + text);
		return null;
	}
}
