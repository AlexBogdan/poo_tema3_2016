
public class DeclareNode extends Node {
	
	DeclareNode(String varName){
		super("DeclareNode");
		LvalNode varNode = new LvalNode(varName);
		varNode.setFather(this);
		this.addNode(varNode);
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}

}
