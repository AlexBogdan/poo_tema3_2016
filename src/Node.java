import java.util.*;

public class Node implements Visitable {
	
	protected  LinkedList<Node> nodes;
	protected String nodeName = "New node";
	protected Node tataLaNod = null;
	protected boolean isReallyVisitable = true;
	
	Node(String nodeName){
		nodes = new LinkedList<Node>();
		this.nodeName = nodeName;
	}
	
	public void close(){
		isReallyVisitable = false;
	}
	
	public void open(){
		isReallyVisitable = true;
	}
	
	public boolean isVisitable(){
		return isReallyVisitable;
	}
	
	public void setFather(Node node){
		this.tataLaNod = node;
	}
	
	public Node getFather(){
		return tataLaNod;
	}
	
	public String getNodeName(){
		return nodeName;
	}
	
	public LinkedList<Node> getNodes(){
		return nodes;
	}
	
	public void addNode(Node node){
		nodes.add(node);
	}
	
	public void accept(Visitor v) {
		// TODO Auto-generated method stub
		
	}

}
