/**
 * TreeVisitor nu va implementa un Visitor pentru ca el face exact acelasi lucru
 * pentru orice fel de node, anume viziteaza nodul curent si dupa lista sa de noduri.
 * @author arbiter
 *
 */

public class TreeVisitor implements Visitor {
	private Visitor v;
	private static int noTabs;

	TreeVisitor(Visitor v){
		this.v = v;
		noTabs = 0;
	}
	
	public void visitNode(Node n){
		n.accept(v);
		if (n instanceof WhileNode && ((WhileNode) n).getRepeat() == false 
				&& v instanceof InterpretVisitor){
			return;
		}
		noTabs ++;
		for (Node node : n.getNodes()){
			node.accept(this);
		}
		noTabs --;
	}
	
	public static int getNoTabs(){
		return noTabs;
	}

	@Override
	public void visit(MainNode main) {
		if(!main.isVisitable()){
			return;
		}
		visitNode(main);
	}

	@Override
	public void visit(DeclareNode declare) {
		if(!declare.isVisitable()){
			return;
		}
		visitNode(declare);
	}

	@Override
	public void visit(LvalNode left) {
		if(!left.isVisitable()){
			return;
		}
		visitNode(left);
	}

	@Override
	public void visit(ConstantNode constant) {
		if(!constant.isVisitable()){
			return;
		}
		visitNode(constant);
	}

	@Override
	public void visit(AssignmentNode assignment) {
		if(!assignment.isVisitable()){
			return;
		}
		visitNode(assignment);
	}

	@Override
	public void visit(PrintNode print) {
		if(!print.isVisitable()){
			return;
		}
		visitNode(print);
	}

	@Override
	public void visit(StringNode string) {
		if(!string.isVisitable()){
			return;
		}
		visitNode(string);
	}
	
	@Override
	public void visit(VariableNode var) {
		if(!var.isVisitable()){
			return;
		}
		visitNode(var);
	}
	
	@Override
	public void visit(RvalNode right) {
		if(!right.isVisitable()){
			return;
		}
		visitNode(right);
	}

	@Override
	public void visit(IfNode ifNode) {
		if(!ifNode.isVisitable()){
			return;
		}
		visitNode(ifNode);
	}

	@Override
	public void visit(ConditionNode condition) {
		if(!condition.isVisitable()){
			return;
		}
		visitNode(condition);
	}

	@Override
	public void visit(IfBodyNode ifBody) {
		if(!ifBody.isVisitable()){
			return;
		}
		visitNode(ifBody);
	}

	@Override
	public void visit(ElseBodyNode elseBody) {
		if(!elseBody.isVisitable()){
			return;
		}
		visitNode(elseBody);
	}
	
	@Override
	public void visit(OrNode or) {
		if(!or.isVisitable()){
			return;
		}
		visitNode(or);
	}

	@Override
	public void visit(SumNode sum) {
		if(!sum.isVisitable()){
			return;
		}
		visitNode(sum);
	}

	@Override
	public void visit(MultiplicationNode mult) {
		if(!mult.isVisitable()){
			return;
		}
		visitNode(mult);
	}

	@Override
	public void visit(WhileNode whileNode) {
		if(!whileNode.isVisitable()){
			return;
		}
		
		if (v instanceof InterpretVisitor){
			whileNode.setRepeat(true);
		}
		else {
			whileNode.setRepeat(false);
		}
		do{
			visitNode(whileNode);
		}while (whileNode.getRepeat());
	}

	@Override
	public void visit(GreaterThanNode greater) {
		if(!greater.isVisitable()){
			return;
		}
		visitNode(greater);
	}

	@Override
	public void visit(BodyNode body) {
		if(!body.isVisitable()){
			return;
		}
		visitNode(body);
	}

	@Override
	public void visit(EqualToNode equal) {
		if(!equal.isVisitable()){
			return;
		}
		visitNode(equal);
	}

	@Override
	public void visit(ModuloNode modulo) {
		if(!modulo.isVisitable()){
			return;
		}
		visitNode(modulo);
	}

	@Override
	public void visit(AndNode and) {
		if(!and.isVisitable()){
			return;
		}
		visitNode(and);
	}

	@Override
	public void visit(DivisionNode div) {
		if(!div.isVisitable()){
			return;
		}
		visitNode(div);
	}

	@Override
	public void visit(DifferenceNode diff) {
		if(!diff.isVisitable()){
			return;
		}
		visitNode(diff);
	}
}
