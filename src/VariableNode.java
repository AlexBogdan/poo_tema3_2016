
public class VariableNode extends Node {
	
	private String varName;
	
	VariableNode(String name){
		super("VariableNode");
		this.varName = name;
	}
	
	public String getVarName(){
		return varName;
	}
	public void accept (Visitor v){
		v.visit(this);
	}
}
