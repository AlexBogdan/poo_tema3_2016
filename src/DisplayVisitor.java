import java.io.*;

public class DisplayVisitor implements Visitor {

	PrintWriter writer;
	
	DisplayVisitor(String fileName){
		try{
		    writer = new PrintWriter(fileName, "UTF-8");
		} catch (IOException e) {
		   
		}
	}
	
	private void printTabs(){
		for (int i=1 ; i<= TreeVisitor.getNoTabs() ; i++){
			writer.print("\t");
		}
	}
	
	@Override
	public void visit(MainNode main) {
		printTabs();
		writer.println(main.getNodeName());
		writer.flush();
	}

	@Override
	public void visit(DeclareNode declare) {
		printTabs();
		writer.println(declare.getNodeName());
		writer.flush();
	}

	@Override
	public void visit(LvalNode left) {
		printTabs();
		writer.println(left.getNodeName() + " <" + left.getVarName() + ">");
		writer.flush();
	}
	
	@Override
	public void visit(RvalNode right) {
		printTabs();
		writer.println(right.getNodeName() + " <" + right.getVarName() + ">");
		writer.flush();
	}

	@Override
	public void visit(ConstantNode constant) {
		printTabs();
		writer.println(constant.getNodeName() + " <" + constant.getVarValue() + ">");
		writer.flush();
	}

	public void visit(AssignmentNode assignment) {
		printTabs();
		writer.println(assignment.getNodeName());
		writer.flush();
	}
	
	public void visit(PrintNode print){
		printTabs();
		writer.println(print.getNodeName()); 
		writer.flush();
	}
	
	public void visit(StringNode string){
		printTabs();
		writer.println(string.getNodeName() + " <" + string.getText() + ">");
		writer.flush();
	}
	
	public void visit(VariableNode var){
		printTabs();
		writer.println(var.getNodeName() + " <" + var.getVarName() + ">");
		writer.flush();
	}

	@Override
	public void visit(IfNode ifNode) {
		printTabs();
		writer.println(ifNode.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(ConditionNode condition) {
		printTabs();
		writer.println(condition.getNodeName() + " <" + condition.getVarName() + ">");
		writer.flush();
	}

	@Override
	public void visit(IfBodyNode ifBody) {
		printTabs();
		writer.println(ifBody.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(ElseBodyNode elseBody) {
		printTabs();
		writer.println(elseBody.getNodeName()); 
		writer.flush();
	}
	
	@Override
	public void visit(OrNode or) {
		printTabs();
		writer.println(or.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(SumNode sum) {
		printTabs();
		writer.println(sum.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(MultiplicationNode mult) {
		printTabs();
		writer.println(mult.getNodeName()); 
		writer.flush();
	}
	
	@Override
	public void visit(WhileNode whileNode) {
		printTabs();
		writer.println(whileNode.getNodeName()); 
		writer.flush();
	}
	
	@Override
	public void visit(GreaterThanNode greater) {
		printTabs();
		writer.println(greater.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(BodyNode body) {
		printTabs();
		writer.println(body.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(EqualToNode equal) {
		printTabs();
		writer.println(equal.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(ModuloNode modulo) {
		printTabs();
		writer.println(modulo.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(AndNode and) {
		printTabs();
		writer.println(and.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(DivisionNode div) {
		printTabs();
		writer.println(div.getNodeName()); 
		writer.flush();
	}

	@Override
	public void visit(DifferenceNode diff) {
		printTabs();
		writer.println(diff.getNodeName()); 
		writer.flush();
	}
}
