import java.io.*;
import java.util.*;

/**
 *  Clasa principala in care vom implementa efectiv tema
 * @author arbiter
 * baseNode = in el se tine tot arborele <br>
 * currentNode = unde ne aflam in Tree <br>
 * currentStatusFlag = ce se intampla in timpul ultimei executii a comenzii citite <br>
 * currentLevel = pe ce nivel ne aflam in Tree (1 fiind un MainNode) <br>
 */

public class ArnoldSchwarzenegger {
	
	private static Node baseNode;
	private static Node currentNode;
	private static int currentStatusFlag = 0;
	private static int currentLevel = 0;
	
	public static void previous(){
		currentNode = currentNode.getFather();
		currentLevel --;
	}
	
	public static void next(Node node){
		currentNode = node;
		currentLevel ++;
	}
	
	private static void interpretLine(String text){
		setFlag(0);
		Node node = NodeFactory.generateNode(text);
		if (node != null ){
			currentNode.addNode(node);
			node.setFather(currentNode);
		}
		else {
			
			//TODO [Other Node Actions]
			//Using nodeFlag;
			// TODO [END NODE]
			//return ;
		}
		
		if (currentStatusFlag == 1 ){
			next(node);
			return;
		}
		
		if (currentStatusFlag == -1){
			previous();
			return ;
		}
		
		if (currentStatusFlag == 2 ){
			next(node);
			next(currentNode.getNodes().getLast());
			return;
		}
		
		if (currentStatusFlag == -2){
			previous();
			previous();
			return ;
		}
		
		switch (currentStatusFlag){
			case 10:
				currentNode = node;
				currentLevel = 1;
				return;
			case -10: //Se inchide programul
				currentNode = baseNode;
				currentLevel = 0;
				return;
		}
	}

	public static void main (String args[]) throws FileNotFoundException{
		
		// Cateva initializari pentru tema
		baseNode = new Node("ArnoldNode");
		currentNode = baseNode;
		
		args[0] = args[0].replace("\t", "");
		FileReader fileReader =  new FileReader(args[0]);
		Scanner reader = new Scanner(fileReader);
		String line;
		while (reader.hasNext()){
			line = reader.nextLine();
			interpretLine(line);
		}
		reader.close();
		//Afisam Tree-ul
		args[0] = args[0].replaceAll(".ac", "");
		args[0] = args[0].replaceAll("tests/", "");
		DisplayVisitor d = new DisplayVisitor(args[0] + ".ast");
		TreeVisitor tree = new TreeVisitor(d);
		
		for (Node node : baseNode.getNodes()){
			node.accept(tree);
		}
		
		//Interpretam Tree-ul
		InterpretVisitor i = new InterpretVisitor(args[0] + ".out");
		tree = new TreeVisitor(i);
		for (Node node : baseNode.getNodes()){
			node.accept(tree);
		}

	}
	
	public static void setFlag(int i){
		currentStatusFlag = i;
	}
	
	public static int getFlag(){
		return currentStatusFlag;
	}
	
	public static Node getCurrentNode(){
		return currentNode;
	}
}
