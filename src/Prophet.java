/**
 * Prophet returneaza rezultatul unui sir de noduri or/and/aritmetice
 * @author arbiter
 */
public class Prophet {
	
	public static Integer prophecyStart(Node node){
		
		if (node instanceof ConstantNode){
			return ((ConstantNode) node).getVarValue();
		}
		if (node instanceof RvalNode){
			String varName = ((RvalNode) node).getVarName();
			return InterpretVisitor.getVarValue(varName);
		}
		
		Node firstNode = node.getNodes().get(0);
		Node secondNode = node.getNodes().get(1);
		
		if (node instanceof OrNode){
			if (prophecyStart(firstNode) != 0 || prophecyStart(secondNode) !=0){
				return 1;
			}
			else return 0;
		}
		if (node instanceof AndNode){
			if (prophecyStart(firstNode) != 0 && prophecyStart(secondNode) !=0){
				return 1;
			}
			else return 0;
		}
		if (node instanceof EqualToNode){
			if (prophecyStart(firstNode) == prophecyStart(secondNode)){
				return 1;
			}
			else return 0;
		}
		if (node instanceof GreaterThanNode){
			if (prophecyStart(firstNode) > prophecyStart(secondNode)){
				return 1;
			}
			else return 0;
		}
		if (node instanceof SumNode){
			return prophecyStart(firstNode) + prophecyStart(secondNode);
		}
		if (node instanceof DifferenceNode){
			return prophecyStart(firstNode) - prophecyStart(secondNode);
		}
		if (node instanceof MultiplicationNode){
			return prophecyStart(firstNode) * prophecyStart(secondNode);
		}
		if (node instanceof DivisionNode){
			return prophecyStart(firstNode) / prophecyStart(secondNode);
		}
		if (node instanceof ModuloNode){
			return prophecyStart(firstNode) % prophecyStart(secondNode);
		}
		
		return Integer.MAX_VALUE;
	}
}
