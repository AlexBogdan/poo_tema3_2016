
public class MainNode extends Node {
	
	MainNode(){
		super("MainNode");
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
} 
