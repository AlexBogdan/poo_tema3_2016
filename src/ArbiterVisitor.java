/**
 * Schimba stausul unui nod in nevizitabil in cazul in care este trimis acolo
 * @author arbiter
 *
 */
public class ArbiterVisitor implements Visitor {
	
	@Override
	public void visit(MainNode main) {
		main.close();
	}

	@Override
	public void visit(DeclareNode declare) {
		declare.close();
	}

	@Override
	public void visit(LvalNode left) {
		left.close();
	}

	@Override
	public void visit(ConstantNode constant) {
		constant.close();
	}

	@Override
	public void visit(AssignmentNode assignment) {
		assignment.close();
	}

	@Override
	public void visit(PrintNode print) {
		print.close();
	}

	@Override
	public void visit(StringNode string) {
		string.close();
	}

	@Override
	public void visit(VariableNode var) {
		var.close();
	}

	@Override
	public void visit(RvalNode right) {
		right.close();
	}

	@Override
	public void visit(IfNode ifNode) {
		ifNode.close();
	}

	@Override
	public void visit(ConditionNode condition) {
		condition.close();
	}

	@Override
	public void visit(IfBodyNode ifBody) {
		ifBody.close();
	}

	@Override
	public void visit(ElseBodyNode elseBody) {
		elseBody.close();
	}

	@Override
	public void visit(OrNode or) {
		or.close();
	}

	@Override
	public void visit(SumNode sum) {
		sum.close();
	}

	@Override
	public void visit(MultiplicationNode mult) {
		mult.close();
	}

	@Override
	public void visit(WhileNode whileNode) {
		whileNode.close();
	}

	@Override
	public void visit(GreaterThanNode greater) {
		greater.close();
	}

	@Override
	public void visit(BodyNode body) {
		body.close();
	}

	@Override
	public void visit(EqualToNode equal) {
		equal.close();
	}

	@Override
	public void visit(ModuloNode modulo) {
		modulo.close();
	}

	@Override
	public void visit(AndNode and) {
		and.close();
	}

	@Override
	public void visit(DivisionNode div) {
		div.close();
	}

	@Override
	public void visit(DifferenceNode diff) {
		diff.close();
	}

}
