import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class InterpretVisitor implements Visitor {

	private static LinkedList<Variabila> vars;
	public static boolean repeat = false;
	PrintWriter writer;
	
	InterpretVisitor(String fileName){
		try{
		    writer = new PrintWriter(fileName, "UTF-8");
		} catch (IOException e) {
		   
		}
	}
	
	/**
	 * Metoda care returneaza valoarea unei variabile cautate din lista de
	 * variabile vars.
	 * @param varName
	 * @return
	 */
	public static Integer getVarValue(String varName){
		for (Variabila var : vars){
			if (var.getVarName().equals(varName)){
				return var.getVarValue();
			}
		}
		return Integer.MAX_VALUE;
	}
	
	/**
	 * Se initializeaza o lista de variabile
	 * @param main
	 */
	@Override
	public void visit(MainNode main) {
		vars = new LinkedList<Variabila>();
		return;
	}

	/**
	 * Vom extrage din nodurile copii numele si valoarea variabilei
	 */
	@Override
	public void visit(DeclareNode declare) {
		String varName = "NewVar";
		Integer varValue = Integer.MAX_VALUE;
		
		LvalNode firstNode = (LvalNode) declare.getNodes().get(0);
		varName = firstNode.getVarName();
		ConstantNode secondNode = (ConstantNode) declare.getNodes().get(1);
		varValue = secondNode.getVarValue();
		
		vars.add(new Variabila(varName, varValue));
	}

	/**
	 * Nimic de facut (se rezolva in alte metode)
	 */
	@Override
	public void visit(LvalNode left) {
		return;
	}

	/**
	 * Nimic de facut (se rezolva in alte metode)
	 */
	@Override
	public void visit(ConstantNode constant) {
		return;
	}

	/**
	 * TODO
	 */
	@Override
	public void visit(AssignmentNode assignment) {
		String varName = "New Value";
		String secondVarName = "";
		Integer varValue = Integer.MAX_VALUE;
		
		LvalNode firstNode = (LvalNode) assignment.getNodes().get(0);
		varName = firstNode.getVarName();
		Node secondNode = assignment.getNodes().get(1);
		varValue = Prophet.prophecyStart(secondNode);
		
		//Setam noua valoare obtinuta
		for (Variabila var : vars){
			if (var.getVarName().equals(varName)){
				var.setValue(varValue);
				return;
			}
		}
	}

	/**
	 * Se printeaza, in functie de caz, mesajul sau valoarea variabilei
	 */
	@Override
	public void visit(PrintNode print) {
		for(Node node : print.getNodes()){
			if (node instanceof StringNode){
				writer.println(((StringNode) node).getText());
				writer.flush();
				return;
			}
			if (node instanceof VariableNode){
				String varName = ((VariableNode) node).getVarName();
				writer.println(getVarValue(varName));
				writer.flush();
				return;	
			}
		}
	}

	/**
	 * Se rezolva in PrintNode, nimic de facut aici
	 */
	@Override
	public void visit(StringNode string) {
		return;
	}

	/**
	 * Se rezolva in PrintNode, nimic de facut aici
	 */
	@Override
	public void visit(VariableNode var) {
		return;
	}

	/**
	 * Se rezolva in alte metode, nimic de facut aici
	 */
	@Override
	public void visit(RvalNode right) {
		return;
	}

	/**
	 * Verificam daca variabila din condition este sau nu 0 si executam
	 * Body-ul pe care suntem trimisi.
	 */
	@Override
	public void visit(IfNode ifNode) {
		String varName = "";
		Integer varValue = Integer.MAX_VALUE;
		
		ConditionNode firstNode = (ConditionNode) ifNode.getNodes().get(0);
		varName = firstNode.getVarName();
		varValue = getVarValue(varName);
		
		for(Node node : ifNode.getNodes()){
			node.open();
		}
		
		if(varValue == 0){
			ifNode.getNodes().get(1).close();
		}
		else{
			if (ifNode.getNodes().size() > 2)
				ifNode.getNodes().get(2).close();
		}
	}

	@Override
	public void visit(ConditionNode condition) {
		return;
	}

	@Override
	public void visit(IfBodyNode ifBody) {
		return;
	}

	@Override
	public void visit(ElseBodyNode elseBody) {
		return;
	}

	@Override
	public void visit(OrNode or) {
		return;
	}

	@Override
	public void visit(SumNode sum) {
		return;
	}

	@Override
	public void visit(MultiplicationNode mult) {
		return;
	}

	@Override
	public void visit(WhileNode whileNode) {
		//TODO
		String varName;
		Integer varValue;
		ConditionNode firstNode = (ConditionNode) whileNode.getNodes().get(0);
		varName = firstNode.getVarName();
		varValue = getVarValue(varName);
		if (varValue != 0){
			whileNode.setRepeat(true);
		}
		else whileNode.setRepeat(false);
	}

	@Override
	public void visit(GreaterThanNode greater) {
		return;
	}

	@Override
	public void visit(BodyNode body) {
		return;
	}

	@Override
	public void visit(EqualToNode equal) {
		return;
	}

	@Override
	public void visit(ModuloNode modulo) {
		return;
	}

	@Override
	public void visit(AndNode and) {
		return;
	}

	@Override
	public void visit(DivisionNode div) {
		return;
	}

	@Override
	public void visit(DifferenceNode diff) {
		return;
	}

}
