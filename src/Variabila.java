
public class Variabila {
	
	private String varName;
	private Integer varValue;
	
	Variabila (String varName){
		this.varName = varName;
		this.varValue = Integer.MAX_VALUE; // Undefined
	}
	
	Variabila(String varName, Integer varValue){
		this.varName = varName;
		this.varValue = varValue;
	}
	
	public void setValue (Integer varValue){
		this.varValue = varValue;
	}
	
	public String getVarName (){
		return varName;
	}
	
	public Integer getVarValue(){
		return varValue;
	}
}
