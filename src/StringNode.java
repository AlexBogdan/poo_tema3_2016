
public class StringNode extends Node {
	
	private String text;
	
	StringNode(String text){
		super("StringNode");
		this.text = text;
	}
	
	public String getText(){
		return text;
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
