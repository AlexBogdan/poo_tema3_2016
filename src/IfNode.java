
public class IfNode extends Node {
	
	
	IfNode(String varName){
		super("IfNode");
		ConditionNode node = new ConditionNode(varName);
		node.setFather(this);
		this.addNode(node);
		IfBodyNode bodyNode = new IfBodyNode();
		bodyNode.setFather(this);
		this.addNode(bodyNode);
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
