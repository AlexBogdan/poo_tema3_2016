
public class LvalNode extends Node {

	private String varName;
	
	LvalNode(String varName) {
		super("LvalNode");
		this.varName = varName;
	}
	
	public String getVarName(){
		return varName;
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
