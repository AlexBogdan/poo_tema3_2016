
public class AssignmentNode extends Node {
	
	AssignmentNode(String varName){
		super("AssignmentNode");
		LvalNode leftNode = new LvalNode(varName);
		leftNode.setFather(this);
		this.addNode(leftNode);
	}
	
	public void accept(Visitor v){
		v.visit(this);
	}
}
