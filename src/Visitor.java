
public interface Visitor {
	public void visit(MainNode main);
	public void visit(DeclareNode declare);
	public void visit(LvalNode left);
	public void visit(ConstantNode constant);
	public void visit(AssignmentNode assignment);
	public void visit(PrintNode print);
	public void visit(StringNode string);
	public void visit(VariableNode var);
	public void visit(RvalNode right);
	public void visit(IfNode ifNode);
	public void visit(ConditionNode condition);
	public void visit(IfBodyNode ifBody);
	public void visit(ElseBodyNode elseBody);
	public void visit(OrNode or);
	public void visit(SumNode sum);
	public void visit(MultiplicationNode mult);
	public void visit(WhileNode whileNode);
	public void visit(GreaterThanNode greater);
	public void visit(BodyNode body);
	public void visit(EqualToNode equal);
	public void visit(ModuloNode modulo);
	public void visit(AndNode and);
	public void visit(DivisionNode div);
	public void visit(DifferenceNode diff);
}
