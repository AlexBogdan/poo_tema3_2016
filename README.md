-=-=-=-=-=-=-=-== Andrei Bogdan Alexandru - 324CA -=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Tema 3 POO -=-=-=-=-=-=--=-=-=-=-=-=-=-=--=-=-

Pentru rezolvarea temei am implementat toate nodurile arborelui Arnold extinzand
un nod general care implementeaza interfata Visitable si cativa Visitori care sa
parcurga acest arbore

Citind din fisierul de input linie cu linie comenziile se vor interpreta pe rand
generand astfel arborele

TreeVisitor este cel care porneste dintr-un nod si parcurge tot subarborele din
acel nod.

DisplayVisitor va printa arborele nod cu nod

InterpretVisitor va intepreta nodurile ce formeaza impreuna instructiuni.

ArbiterVisitor este cel care inchide nodurile in care este trimis, facandu-le
nevizitabile (asta in cazul if-ului unde vedem daca intram pe body sau pe else)

Prophet care nu este tocmai un visitor, el este apelat intr-un nod si va face
operatiile pe nodurile gasite, recursiv, intorcand rezultatul. El rezolva toate
problemele logice si aritmetice.

Pentru a putea interpreta un arbore vom tine minte si o lista de variabile unde
vom stoca Variabile de forma nume <=> valoare pentru a putea face operatii cu ele,
astfel ca atunci cand un nod are numele unei variabile ii va putea obtine valoarea
sau seta valoarea in aceasta lista (reprezentand memoria)

Mai multe detalii legate de implementare se gasesc in JavaDoc


